using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Box", fileName = "SpaunObject")]
public class BuliteConfige : ScriptableObject
{
    // ���� ���� ��������� ������ �����, � ������� �� ����� �������� ���� �������.
    public List<SpaunObj> SpaunObj;
}

// ���� �� ����� ������� �������� � inspectore.
[System.Serializable]

public class SpaunObj
{
    [SerializeField] private int _number;
    [SerializeField] private GameObject _prefabe;


    // ��������
    // ������ ���������� ��������.
    /*
    public int Number
    {
        get
        {
            return _number;
        }
    }*/

    // �������� / ����
    public int Number => _number;

    // �������� / ����
    public GameObject PrefabObject => _prefabe;
}
