using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullite : MonoBehaviour
{
    [SerializeField] private float _miliSecond = 1f;
    [SerializeField] private float _finalSecond = 1000f;

    [SerializeField] private GameObject _cube;
    private float _speed = 70f;

    /*
    private void Start()
    {
        StartCoroutine("Timer");
    }
    */

    // Update is called once per frame
    void Update()
    {
        Impulse();
        
        _miliSecond += 1;

        if (_miliSecond == _finalSecond)
        {
            this._cube.SetActive(false);
            _miliSecond = 0;
        }
    }
    /*
    IEnumerable Timer()
    {
        //_cube.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        this._cube.SetActive(false);
    }*/

    public void Impulse()
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
    }
}
