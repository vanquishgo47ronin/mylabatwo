using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaunObjects : MonoBehaviour
{
    [SerializeField] private Transform _object;
    [SerializeField] private BuliteConfige _buliteConfige;

    [SerializeField] private GameObject _cube;


    // ������ ����� �� ���������, ��� � ��� BuliteConfige, � ������� �� �������� �������� �� GameConfiga.
    [SerializeField] private List<GameObject> _obj;

    private void Awake()
    {
        Initialization();
    }

    // ��������.
    private void Initialization()
    {
        // �������� ���� �� ������� BULITConfig  � �������� ��� � ���������.
        foreach (SpaunObj obj in _buliteConfige.SpaunObj)
        {
            for (int i = 0; i < obj.Number; i++)
            {                                      //�������� (�� ��������� ��� ��������), 
                GameObject _gameObject = Instantiate(obj.PrefabObject, _object);

                // ���� � ���������� ����� � ��������� (SpaunObjects).
                _obj.Add(_gameObject);

                // � ��������� �������
                _gameObject.SetActive(false);
            }
        }
    }

    // ���������� ��� ������ �������.
    public GameObject GetBul(string tag)
    {
        // �������� ������ � ����
        foreach (GameObject _objectFromPool in _obj)
        {
            // �������� �� ���� � ������� ��� �������� ������.
            if (_objectFromPool.tag.Equals(tag) && !_objectFromPool.gameObject.activeInHierarchy)
            {
                // ������� �������.
                _objectFromPool.transform.position = Input.mousePosition;

                _objectFromPool.SetActive(true);
                return _objectFromPool;
            }
        }
        return null;
    }
}
